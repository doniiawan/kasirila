package com.example.kasirila;

import androidx.appcompat.app.AppCompatActivity;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.example.kasirila.app.AppController;
import com.google.android.material.textfield.TextInputLayout;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import static com.example.kasirila.R.id.edusername;

public class LoginActivity extends AppCompatActivity {

    ProgressDialog pDialog;
    Intent intent;

    int success;
    ConnectivityManager conMgr;

    private String url = Server.URL + "login.php";

    private static final String TAG = LoginActivity.class.getSimpleName();

    private static final String TAG_SUCCESS = "success";
    private static final String TAG_MESSAGE = "message";

    String tag_json_obj = "json_obj_req";
    public final static String TAG_ID = "id";
    public final static String TAG_IDTOKO = "idtoko";
    public final static String TAG_NAMATOKO = "namatoko";
    public final static String TAG_NAMA = "nama";
    public final static String TAG_NOKTP = "nik";
    public final static String TAG_ALAMAT = "alamat";
    public final static String TAG_TELP = "telp";
    public final static String TAG_TARGET = "target";
    AlertDialog.Builder dialog;

    SharedPreferences sharedpreferences;
    Boolean session = false;
    String id, username;
    public static final String my_shared_preferences = "my_shared_preferences";
    public static final String session_status = "session_status";


    EditText edusername,edpassword;
    Button btnsignin;
    TextView tvforgot;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        findViewById();

        conMgr = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        {
            if (conMgr.getActiveNetworkInfo() != null
                    && conMgr.getActiveNetworkInfo().isAvailable()
                    && conMgr.getActiveNetworkInfo().isConnected()) {
            } else {
                Toast.makeText(getApplicationContext(), "No Internet Connection",
                        Toast.LENGTH_LONG).show();
            }
        }

        sharedpreferences = getSharedPreferences(MainActivity.my_shared_preferences, Context.MODE_PRIVATE);
        btnsignin.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                String username = edusername.getText().toString();
                String password = edpassword.getText().toString();

                // mengecek kolom yang kosong
                if (username.trim().length() > 0 && password.trim().length() > 0) {
                    if (conMgr.getActiveNetworkInfo() != null
                            && conMgr.getActiveNetworkInfo().isAvailable()
                            && conMgr.getActiveNetworkInfo().isConnected()) {
                        checkLogin(username, password);
                    } else {
                        Toast.makeText(getApplicationContext() ,"No Internet Connection", Toast.LENGTH_LONG).show();
                    }
                } else {
                    // Prompt user to enter credentials
                    Toast.makeText(getApplicationContext() ,"Kolom tidak boleh kosong", Toast.LENGTH_LONG).show();
                }
            }
        });

    }

    private void checkLogin(final String username, final String password) {
        pDialog = new ProgressDialog(this);
        pDialog.setCancelable(false);
        pDialog.setMessage("Logging in ...");
        showDialog();

        StringRequest strReq = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.e(TAG, "Login Response: " + response.toString());
                hideDialog();

                try {
                    JSONObject jObj = new JSONObject(response);
                    success = jObj.getInt(TAG_SUCCESS);

                    // Check for error node in json
                    if (success == 1) {

                    // Check for error node in json
                    String id = jObj.getString(TAG_ID);
                    String idtoko = jObj.getString(TAG_IDTOKO);
                    String namatoko = jObj.getString(TAG_NAMATOKO);
                    String nama = jObj.getString(TAG_NAMA);
                    String alamat = jObj.getString(TAG_ALAMAT);
                    String telp = jObj.getString(TAG_TELP);
                    String noKTP = jObj.getString(TAG_NOKTP);
                    String target = jObj.getString(TAG_TARGET);

                    Log.e("Successfully Login!", jObj.toString());

                    //Toast.makeText(getApplicationContext(), jObj.getString(TAG_MESSAGE), Toast.LENGTH_LONG).show();
                    //Toast.makeText(getApplicationContext(), Username + "..." + Email + "..." + Alamat + "..." + Telp + "..." +  NoKTP, Toast.LENGTH_LONG).show();

                    SharedPreferences.Editor editor = sharedpreferences.edit();
                    editor.putBoolean(MainActivity.session_status, true);
                    editor.putString(TAG_ID, id);
                    editor.putString(TAG_IDTOKO, idtoko);
                    editor.putString(TAG_NAMATOKO, namatoko);
                    editor.putString(TAG_NAMA, nama);
                    editor.putString(TAG_ALAMAT, alamat);
                    editor.putString(TAG_TELP, telp);
                    editor.putString(TAG_NOKTP, noKTP);
                    editor.putString(TAG_TARGET,target);
                    editor.commit();

                    Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                    finish();
                    startActivity(intent);

                    } else {
                        Toast.makeText(getApplicationContext(),
                                jObj.getString(TAG_MESSAGE), Toast.LENGTH_LONG).show();

                    }
                } catch (JSONException e) {
                    // JSON error
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "Login Error: " + error.getMessage());
                Toast.makeText(getApplicationContext(),
                        error.getMessage(), Toast.LENGTH_LONG).show();

                hideDialog();

            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to login url
                Map<String, String> params = new HashMap<String, String>();
                params.put("username", username);
                params.put("password", password);

                return params;
            }

        };

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq, tag_json_obj);
    }

    private void showDialog() {
        if (!pDialog.isShowing())
            pDialog.show();
    }

    private void hideDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }
    private void findViewById() {
        edusername = findViewById(R.id.edusername);
        edpassword = findViewById(R.id.edpassword);
        btnsignin = findViewById(R.id.btnSignin);
        tvforgot = findViewById(R.id.tvforgot);
    }
}
