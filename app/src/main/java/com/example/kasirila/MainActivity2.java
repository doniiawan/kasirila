package com.example.kasirila;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.example.kasirila.Adapter.MenuAdapter;
import com.example.kasirila.Adapter.MenuAdapter2;
import com.example.kasirila.Data.MenuData;
import com.example.kasirila.app.AppController;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class MainActivity2 extends AppCompatActivity {
    String email, username, noktp, alamat, telp, userid, target, idtoko, namatoko, date, kdbln, kdhari, kdlast;
    private static String url_code= Server.URL + "generatecode.php";
    ConnectivityManager conMgr;
    ProgressDialog pDialog;
    String tag_json_obj = "json_obj_req";
    AlertDialog.Builder dialog;
    LayoutInflater inflater;
    EditText ednama,edketerangan,edbayar;
    Button btnsimpan;
    public TextView tvtotal;
    RecyclerView recyclerView;
    ArrayList<MenuData> itemList = new ArrayList<MenuData>();
    private String url_select = Server.URL + "selectmenu.php?idtoko=";
    MenuAdapter adapter;
    Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);

        Intent objIntent = getIntent();
        userid = objIntent.getStringExtra("id");
        idtoko = objIntent.getStringExtra("idtoko");
        namatoko = objIntent.getStringExtra("namatoko");
        username = objIntent.getStringExtra("nama");
        noktp = objIntent.getStringExtra("nik");
        alamat = objIntent.getStringExtra("alamat");
        telp = objIntent.getStringExtra("telp");
        target = objIntent.getStringExtra("target");
        findViewById();

        conMgr = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        {
            if (conMgr.getActiveNetworkInfo() != null
                    && conMgr.getActiveNetworkInfo().isAvailable()
                    && conMgr.getActiveNetworkInfo().isConnected()) {
            } else {
                Toast.makeText(getApplicationContext(), "No Internet Connection",
                        Toast.LENGTH_LONG).show();
            }
        }

        final hitungpesanan hitungpesanan = new hitungpesanan();
        tvtotal.setText("9");
        adapter = new MenuAdapter(itemList);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(MainActivity2.this);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(adapter);
        itemList.clear();
        adapter.notifyDataSetChanged();
        refreshlist();
        btnsimpan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            }
        });

    }

    public void refreshlist() {
        itemList.clear();
        adapter.notifyDataSetChanged();

        // membuat request JSON
        JsonArrayRequest jArr = new JsonArrayRequest(url_select+idtoko, new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {
                // Parsing json
                for (int i = 0; i < response.length(); i++) {
                    try {
                        JSONObject obj = response.getJSONObject(i);

                        MenuData item = new MenuData();

                        item.setKode(obj.getString("id"));
                        item.setNama(obj.getString("nama"));
                        item.setHarga(obj.getString("harga"));

                        // menambah item ke array
                        itemList.add(item);

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                // notifikasi adanya perubahan data pada adapter
                adapter.notifyDataSetChanged();

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });

        // menambah request ke request queue
        AppController.getInstance().addToRequestQueue(jArr);
    }

    public void findViewById() {
        ednama = findViewById(R.id.ednama);
        edketerangan = findViewById(R.id.edketerangan);
        edbayar = findViewById(R.id.edbayar);
        tvtotal = findViewById(R.id.tvtotal);
        btnsimpan = findViewById(R.id.btnsimpan);
        recyclerView = findViewById(R.id.recycler);
    }


}