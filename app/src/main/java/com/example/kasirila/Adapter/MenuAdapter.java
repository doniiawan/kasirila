package com.example.kasirila.Adapter;

import android.app.Activity;
import android.content.Context;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.RecyclerView.ViewHolder;

import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.example.kasirila.Data.MenuData;
import com.example.kasirila.MainActivity;
import com.example.kasirila.R;
import com.example.kasirila.MainActivity2;
import com.example.kasirila.hitungpesanan;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;


public class MenuAdapter extends RecyclerView.Adapter<MenuAdapter.MenuViewHolder> {
    MainActivity2 mainActivity2 = new MainActivity2();
    private ArrayList<MenuData> datalist;
    hitungpesanan hitungpesanan = new hitungpesanan();
    public MenuAdapter(ArrayList<MenuData> datalist) {
        this.datalist = datalist;
    }

    public MenuAdapter() {
    }

    @Override
    public MenuViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View view = layoutInflater.inflate(R.layout.view_produk, parent, false);
        return new MenuViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final MenuViewHolder holder, final int position) {

        final MenuData dataList = datalist.get(position);
        holder.kode.setText(dataList.getKode());
        holder.nama.setText(dataList.getNama());
        holder.harga.setText(dataList.getHarga());
        holder.tvjumlah.setText(String.valueOf(dataList.getJumlah()));

        holder.tvplus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hitungpesanan.setTotalPlus(Integer.parseInt(dataList.getHarga()));
                dataList.setJumlah2();
                holder.tvjumlah.setText(String.valueOf(dataList.getJumlah()));
                Toast.makeText(v.getContext(),"Total "+String.valueOf(hitungpesanan.getTotal()),Toast.LENGTH_SHORT).show();

            }
        });

        holder.tvminus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(dataList.getJumlah()==0){
                    Toast.makeText(v.getContext(),"Jumlah Tidak Bisa Minus",Toast.LENGTH_SHORT).show();
                } else {

                    hitungpesanan.setTotalMinus(Integer.parseInt(dataList.getHarga()));
                    datalist.get(position).setJumlah3();
                    holder.tvjumlah.setText(String.valueOf(dataList.getJumlah()));
                    Toast.makeText(v.getContext(),"Total "+String.valueOf(hitungpesanan.getTotal()),Toast.LENGTH_SHORT).show();


                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return (datalist != null) ? datalist.size() : 0;
    }

    public class MenuViewHolder extends RecyclerView.ViewHolder {
        private TextView kode,nama,harga,tvplus,tvminus,tvjumlah,tvtotal;
        public MenuViewHolder(View itemView) {
            super(itemView);
            kode = (TextView) itemView.findViewById(R.id.kode);
            nama = (TextView) itemView.findViewById(R.id.nama);
            harga = (TextView) itemView.findViewById(R.id.harga);
            tvplus = (TextView) itemView.findViewById(R.id.tvplus);
            tvminus = (TextView) itemView.findViewById(R.id.tvminus);
            tvjumlah = (TextView) itemView.findViewById(R.id.tvjumlah);
        }
    }

}
