package com.example.kasirila.Adapter;

import android.app.Activity;
import android.content.Context;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.RecyclerView.ViewHolder;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.example.kasirila.Data.MenuData;
import com.example.kasirila.Data.MenuData2;
import com.example.kasirila.R;
import com.example.kasirila.MainActivity2;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;


public class MenuAdapter2 extends BaseAdapter {
    private AppCompatActivity activity;
    private LayoutInflater inflater;
    private List<MenuData2> items;

    public MenuAdapter2(AppCompatActivity activity, List<MenuData2> items) {
        this.activity = activity;
        this.items = items;
    }

    @Override
    public int getCount() {
        return items.size();
    }

    @Override
    public Object getItem(int location) {
        return items.get(location);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        if (inflater == null)
            inflater = (LayoutInflater) activity
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        if (convertView == null)
            convertView = inflater.inflate(R.layout.view_produk2, null);

        TextView kode = (TextView) convertView.findViewById(R.id.kode);
        TextView harga = (TextView) convertView.findViewById(R.id.harga);
        TextView nama = (TextView) convertView.findViewById(R.id.nama);

        MenuData2 data = items.get(position);

        kode.setText(data.getKode());
        harga.setText(data.getHarga());
        nama.setText(data.getNama());

        return convertView;
    }
}