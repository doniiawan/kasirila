package com.example.kasirila.Data;


public class MenuData2 {
    private String kode,nama,harga;
    private int jumlah=0;

    public MenuData2(){
    }

    public MenuData2(String kode, String nama, String harga, int jumlah) {
        this.kode = kode;
        this.nama = nama;
        this.harga = harga;
        this.jumlah = jumlah;
    }

    public int getJumlah() {
        return jumlah;
    }

    public void setJumlah(int jumlah) {
        this.jumlah = jumlah;

    }

    public void setJumlah2() {
        this.jumlah += 1;
    }
    public void setJumlah3() {
        this.jumlah -= 1;
    }

    public String getKode() {
        return kode;
    }

    public void setKode(String kode) {
        this.kode = kode;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getHarga() {
        return harga;
    }

    public void setHarga(String harga) {
        this.harga = harga;
    }
}
