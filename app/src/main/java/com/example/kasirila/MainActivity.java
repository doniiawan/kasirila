package com.example.kasirila;

import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.example.kasirila.app.AppController;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class MainActivity extends AppCompatActivity {

    int success;
    ConnectivityManager conMgr;
    int logstatus = 0;
    ProgressDialog pDialog;
    private static final String TAG = MainActivity.class.getSimpleName();

    private static final String TAG_SUCCESS = "success";
    private static final String TAG_MESSAGE = "message";
    private static String url_code= Server.URL + "generatecode.php";
    private String url = Server.URL + "omzetharian.php";

    String tag_json_obj = "json_obj_req";
    SharedPreferences sharedpreferences;
    Boolean session = false;
    public final static String TAG_ID = "id";
    public final static String TAG_IDTOKO = "idtoko";
    public final static String TAG_NAMATOKO = "namatoko";
    public final static String TAG_NAMA = "nama";
    public final static String TAG_NOKTP = "nik";
    public final static String TAG_ALAMAT = "alamat";
    public final static String TAG_TELP = "telp";
    public final static String TAG_TARGET = "target";
    String email, username , noktp,alamat,telp, userid,target,idtoko,namatoko,date,kdbln,kdhari,kdlast;
    public static final String my_shared_preferences = "my_shared_preferences";
    public static final String session_status = "session_status";
    TextView tvnama,tvtanggal;
    public TextView tvtotal;
    ImageView ivsetting;
    AlertDialog.Builder dialog;
    LayoutInflater inflater;
    View dialogView;
    CardView cardpesanan,cardhistory,cardproduk;

    private static BluetoothSocket btsocket;
    private static OutputStream outputStream;
    BluetoothAdapter mBluetoothAdapter;
    BluetoothSocket mmSocket;
    BluetoothDevice mmDevice;

    // needed for communication to bluetooth device / network
    OutputStream mmOutputStream;
    InputStream mmInputStream;
    Thread workerThread;

    byte[] readBuffer;
    int readBufferPosition;
    volatile boolean stopWorker;
    int no;
    int resume = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        conMgr = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        {
            if (conMgr.getActiveNetworkInfo() != null
                    && conMgr.getActiveNetworkInfo().isAvailable()
                    && conMgr.getActiveNetworkInfo().isConnected()) {
            } else {
                Toast.makeText(getApplicationContext(), "No Internet Connection",
                        Toast.LENGTH_LONG).show();
            }
        }

        tvtotal = findViewById(R.id.tvtotal);

        SimpleDateFormat sdf2 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        final String currentDate2 = sdf2.format(new Date());

        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
        final String currentDate = sdf.format(new Date());

        findViewById();

        tvtanggal.setText(currentDate);

        // Cek session login jika TRUE maka langsung buka MainActivity
        sharedpreferences = getSharedPreferences(my_shared_preferences, Context.MODE_PRIVATE);
        session = sharedpreferences.getBoolean(session_status, false);
        userid = sharedpreferences.getString(TAG_ID,null);
        idtoko = sharedpreferences.getString(TAG_IDTOKO,null);
        namatoko = sharedpreferences.getString(TAG_NAMATOKO,null);
        username = sharedpreferences.getString(TAG_NAMA, null);
        noktp = sharedpreferences.getString(TAG_NOKTP, null);
        alamat = sharedpreferences.getString(TAG_ALAMAT, null);
        telp = sharedpreferences.getString(TAG_TELP, null);
        target = sharedpreferences.getString(TAG_TARGET,null);

        if (session) {
            tvnama.setText(namatoko);
            cekomzetharian();
            //tvusername.setText(username);
            //tvusername2.setText(noktp);
            //btnlogin.setVisibility(View.INVISIBLE);
            //btnlogout.setVisibility(View.VISIBLE);
            //tvlogout.setText("LOGOUT");
//            try {
//                findBT();
//                openBT();
//            } catch (IOException e) {
//                e.printStackTrace();
//            }
        } else {
            Intent intent = new Intent(MainActivity.this, LoginActivity.class);
            finish();
            startActivity(intent);
        }

        ivsetting.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DialogForm();
            }
        });

        cardpesanan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent ni;
                ni = new Intent(MainActivity.this,MainActivity2.class);
                Bundle b = new Bundle();
                b.putString("id",userid);
                b.putString("idtoko", idtoko);
                b.putString("namatoko", namatoko);
                b.putString("nama", username);
                b.putString("nik", noktp);
                b.putString("alamat", alamat);
                b.putString("telp", telp);
                b.putString("target", target);
                ni.putExtras(b);
                startActivityForResult(ni,1);
            }
        });
    }

    private void cekomzetharian() {
        pDialog = new ProgressDialog(this);
        pDialog.setCancelable(false);
        pDialog.setMessage("Cek Omzet ...");
        showDialog();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        final String currentDate = sdf.format(new Date());
        final DecimalFormat df = new DecimalFormat("###,###,###,###,###.##");

        StringRequest strReq = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.e(TAG, "Login Response: " + response.toString());
                hideDialog();

                try {
                    JSONObject jObj = new JSONObject(response);

                    // Check for error node in json
                    String total = jObj.getString("total");
                    tvtotal.setText("RP. " +String.valueOf(df.format(Double.parseDouble(total)))+",-");



                } catch (JSONException e) {
                    // JSON error
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "Login Error: " + error.getMessage());
                Toast.makeText(getApplicationContext(),
                        error.getMessage(), Toast.LENGTH_LONG).show();

                hideDialog();

            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to login url
                Map<String, String> params = new HashMap<String, String>();
                params.put("id", idtoko);
                params.put("tanggal",currentDate);

                return params;
            }

        };

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq, tag_json_obj);
    }

    private void DialogForm() {

        androidx.appcompat.app.AlertDialog.Builder dialog = new androidx.appcompat.app.AlertDialog.Builder(MainActivity.this);
        LayoutInflater inflater = getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.opsimain, null);
        dialog.setView(dialogView);
        dialog.setCancelable(true);
        dialog.setTitle(" Pilih Menu : ");

        Button btnedit = (Button) dialogView.findViewById(R.id.btnedit);
        Button btninsert = (Button) dialogView.findViewById(R.id.btninsert);
        Button btnmenu = (Button) dialogView.findViewById(R.id.btnmenu);
        Button btnlogout = (Button) dialogView.findViewById(R.id.btnlogout);

        final androidx.appcompat.app.AlertDialog alert = dialog.create();
        alert.setView(dialogView);

        btnedit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Intent ni;
//                ni = new Intent(MainActivity.this,CheckIn.class);
//                Bundle b = new Bundle();
//                b.putString("devid",devid);
//                b.putString("salesid", salesid);
//                b.putString("area", area);
//                b.putString("divisi", divisi);
//                b.putString("area2", area2);
//                b.putString("status","lihat");
//                ni.putExtras(b);
//                startActivityForResult(ni,9);
//                alert.dismiss();
            }
        });

        btninsert.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Intent tbIntent = new Intent(MainActivity.this, NavProductActivity.class);
//                Bundle b = new Bundle();
//                b.putString("divisi", divisi);
//                b.putString("disc", "0");
//                b.putString("area", area);
//                b.putString("kdnota"," ");
//                b.putString("status","lihat");
//                b.putString("program","normal");
//                tbIntent.putExtras(b);
//                startActivityForResult(tbIntent, 9);
//                alert.dismiss();
            }
        });

        btnmenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent ni = null;
                ni = new Intent(MainActivity.this, UbahMenuActivity.class);
                Bundle b = new Bundle();
                b.putString("userid", userid);
                b.putString("idtoko", idtoko);
                b.putString("username",username);
                ni.putExtras(b);
                startActivityForResult(ni, 2);
                alert.dismiss();
            }
        });

        btnlogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedPreferences.Editor editor = sharedpreferences.edit();
                editor.putBoolean(LoginActivity.session_status, false);
                editor.putString(TAG_NAMA, null);
                editor.commit();

                Intent intent = new Intent(MainActivity.this, LoginActivity.class);
                finish();
                startActivity(intent);
            }
        });

        alert.show();
    }

    private void showDialog() {
        if (!pDialog.isShowing())
            pDialog.show();
    }

    private void hideDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }

    private void findViewById() {
        tvnama = findViewById(R.id.tvnama);
        tvtanggal = findViewById(R.id.tvtanggal);
        cardpesanan = findViewById(R.id.cardpesanan);
        cardhistory = findViewById(R.id.cardhistory);
        cardproduk = findViewById(R.id.cardproduk);
        ivsetting = findViewById(R.id.ivsetting);
    }
}