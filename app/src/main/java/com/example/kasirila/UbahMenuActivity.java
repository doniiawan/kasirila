package com.example.kasirila;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.StringRequest;
import com.example.kasirila.Adapter.MenuAdapter;
import com.example.kasirila.Adapter.MenuAdapter2;
import com.example.kasirila.Data.MenuData;
import com.example.kasirila.Data.MenuData2;
import com.example.kasirila.app.AppController;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class UbahMenuActivity extends AppCompatActivity {
    ConnectivityManager conMgr;
    ProgressDialog pDialog;
    int success = 0;
    private String url = Server.URL + "omzetharian.php";
    private static String url_code= Server.URL + "generatemenu.php";
    String tag_json_obj = "json_obj_req";
    String idtoko, userid,username,kodemenu,namamenu,hargamenu,kdlast ;
    AlertDialog.Builder dialog;
    ListView lv;
    ArrayList<MenuData2> itemList = new ArrayList<MenuData2>();
    private String url_select = Server.URL + "selectmenu.php?idtoko=";
    private String url_update = Server.URL + "updatemenu.php";
    MenuAdapter2 adapter;
    Context context;
    Button btntambah;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ubah_menu);
        Intent objIntent = getIntent();
        userid = objIntent.getStringExtra("userid");
        idtoko = objIntent.getStringExtra("idtoko");
        username = objIntent.getStringExtra("username");
        findViewById();

        conMgr = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        {
            if (conMgr.getActiveNetworkInfo() != null
                    && conMgr.getActiveNetworkInfo().isAvailable()
                    && conMgr.getActiveNetworkInfo().isConnected()) {
            } else {
                Toast.makeText(getApplicationContext(), "No Internet Connection",
                        Toast.LENGTH_LONG).show();
            }
        }
        adapter = new MenuAdapter2(UbahMenuActivity.this, itemList);
        lv.setAdapter(adapter);
        itemList.clear();
        adapter.notifyDataSetChanged();
        refreshlist();

        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(final AdapterView<?> parent, View view,
                                    int position, long kdnota) {
                // TODO Auto-generated method stub
                final String kode = itemList.get(position).getKode();
                final String harga = itemList.get(position).getHarga();
                final String nama = itemList.get(position).getNama();

                DialogForm(kode,nama,harga);

            }
        });

        btntambah.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                checkKode(idtoko);
            }
        });
    }

    private void DialogForm(final String kode,final String nama,final String harga) {

        androidx.appcompat.app.AlertDialog.Builder dialog = new androidx.appcompat.app.AlertDialog.Builder(UbahMenuActivity.this);
        LayoutInflater inflater = getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.opsimenu, null);
        dialog.setView(dialogView);
        dialog.setCancelable(true);
        dialog.setTitle("Menu : " + nama);

        Button btnubah = (Button) dialogView.findViewById(R.id.btnubah);
        Button btnhapus = (Button) dialogView.findViewById(R.id.btnhapus);

        final androidx.appcompat.app.AlertDialog alert = dialog.create();
        alert.setView(dialogView);

        btnubah.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DialogForm2(kode,nama,harga,"ubah");
                alert.dismiss();
            }
        });

        btnhapus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DialogForm3(kode,nama);
                alert.dismiss();
            }
        });

        alert.show();
    }

    private void DialogForm2(final String kode,final String nama,final String harga,final String status) {

        androidx.appcompat.app.AlertDialog.Builder dialog = new androidx.appcompat.app.AlertDialog.Builder(UbahMenuActivity.this);
        LayoutInflater inflater = getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.opsientrymenu, null);
        dialog.setView(dialogView);
        dialog.setCancelable(true);
        dialog.setTitle("UBAH MENU");

        final EditText tvharga = (EditText) dialogView.findViewById(R.id.tvharga);
        final EditText tvnama = (EditText) dialogView.findViewById(R.id.tvnama);
        final TextView tvkode = (TextView) dialogView.findViewById(R.id.tvkode);

        tvkode.setText(kode);
        tvnama.setText(nama);
        tvharga.setText(harga);

        dialog.setPositiveButton("SIMPAN", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                updatemenu(tvkode.getText().toString(),tvnama.getText().toString(),tvharga.getText().toString(),status);
                dialog.dismiss();
            }
        });

        dialog.setNegativeButton("BATAL", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        dialog.show();;
    }

    private void DialogForm3(final String kode, final String menu) {
        dialog = new AlertDialog.Builder(UbahMenuActivity.this);
        dialog.setCancelable(true);
        dialog.setMessage("Anda yakin ingin menghapus menu "+ menu + " dari daftar menu?");
        dialog.setTitle("PERINGATAN !!");
        dialog.setPositiveButton("YA", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                updatemenu(kode,menu,"0","hapus");
                dialog.dismiss();
            }
        });

        dialog.setNegativeButton("BATAL", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        dialog.show();
    }

    public void refreshlist() {
        itemList.clear();
        adapter.notifyDataSetChanged();

        // membuat request JSON
        JsonArrayRequest jArr = new JsonArrayRequest(url_select+idtoko, new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {
                // Parsing json
                for (int i = 0; i < response.length(); i++) {
                    try {
                        JSONObject obj = response.getJSONObject(i);

                        MenuData2 item = new MenuData2();

                        item.setKode(obj.getString("id"));
                        item.setNama(obj.getString("nama"));
                        item.setHarga(obj.getString("harga"));

                        // menambah item ke array
                        itemList.add(item);

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                // notifikasi adanya perubahan data pada adapter
                adapter.notifyDataSetChanged();

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });

        // menambah request ke request queue
        AppController.getInstance().addToRequestQueue(jArr);
    }

    private void
    updatemenu(final String kode,final String namamenu,final String harga,final String status) {

        StringRequest strReq = new StringRequest(Request.Method.POST, url_update, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {

                try {
                    JSONObject jObj = new JSONObject(response);
                    success = jObj.getInt("success");
                    // Check for error node in json
                    if (success == 1) {
                        Toast.makeText(UbahMenuActivity.this,jObj.getString("message"), Toast.LENGTH_LONG).show();
                        refreshlist();
                    } else {
                        Toast.makeText(UbahMenuActivity.this,jObj.getString("message"), Toast.LENGTH_LONG).show();
                    }

                    success = 0;

                } catch (JSONException e) {
                    // JSON error
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to login url
                Map<String, String> params = new HashMap<String, String>();
                params.put("status", status);
                params.put("kode", kode);
                params.put("nama", namamenu);
                params.put("harga", harga);
                params.put("id", userid);
                params.put("idtoko", idtoko);
                params.put("username", username);

                return params;
            }

        };

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq, tag_json_obj);
    }

    private void checkKode(final String kode) {
        pDialog = new ProgressDialog(this);
        pDialog.setCancelable(false);
        pDialog.setMessage("Generate Code ...");

        showDialog();
        StringRequest strReq = new StringRequest(Request.Method.POST, url_code, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                hideDialog();
                try {
                    JSONObject jObj = new JSONObject(response);
                    success = jObj.getInt("success");
                    // Check for error node in json
                    if (success == 1) {
                        //Toast.makeText(TagihanActivity.this,"Terpanggil 1", Toast.LENGTH_LONG).show();

                        String last = jObj.getString("last");
                        if (last.length()==1){kdlast="00"+last;}
                        if (last.length()==2){kdlast="0"+last;}
                        if (last.length()==3){kdlast=last;}

                    } else {
                        kdlast="001";
                        //Toast.makeText(TagihanActivity.this,"Terpanggil", Toast.LENGTH_LONG).show();
                    }

                    final String kdnotaxx = "MEN" + kdlast;

                    DialogForm2(kdnotaxx,"","","tambah");

                    //insert_master(kdnotax,currentDate2,NoPenagihan,salesid,kdplg,edjumlah.getText().toString(),nmplg,currentDate,CaraBayar);
                    //KodeNP = kdnotax;

                    success =0;

                } catch (JSONException e) {
                    // JSON error
                    e.printStackTrace();
                }
                //KodeNP=kdnotaxx;
                //Toast.makeText(TagihanActivity.this,"Terpanggil "+KodeNP, Toast.LENGTH_LONG).show();

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                 Toast.makeText(getApplicationContext(),error.getMessage(), Toast.LENGTH_LONG).show();

                hideDialog();
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to login url
                Map<String, String> params = new HashMap<String, String>();
                params.put("id", idtoko);

                return params;
            }

        };

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq, tag_json_obj);
    }
    private void showDialog() {
        if (!pDialog.isShowing())
            pDialog.show();
    }

    private void hideDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }
    public void findViewById() {
        btntambah = findViewById(R.id.btntambah);
        lv = findViewById(R.id.lv);
    }

}